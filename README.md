# SYLT - SYstem for Low-cost Tickets
# application portal for the refund of semester ticket fees on the occasion of the 9-euro-ticket

This application is used to manage the refunds of students who paid for the semesterticket in summer of 2022 and were elegible for a refund due to the german 9-euro-ticket. It was primarily developed for the TU Darmstadt and has some unique features which can easily be patched out. For example the TU Darmstadt had two kinds of ticketowners because medical engineering students paid a higher semesterticket-fee and therefore received a higher refund.

The application is developed in python with django as its framework and is available with german and english language (switchable and autodetected). It also uses bulma css with some small implementations as a design basis.

## Description

The basic functionality is to collect sepa data (account owner name, iban and bic) of students that are elegible for a refund. The backend transforms this data into sepa wire transfers which can be imported into regular banking software.

To figure out who is elegible the system uses some authorization and authentication features. The whole process is based on the Shibboleth-SSO-IDP of the university. This way only university members are even able to log into the first step of the process. In the next step a list of enrolled students is required. This list was provided by the university and contained matriculation number, full name and if the student was enrolled in medical engineering or not (boolean). Some students who were theoretically eligible for a refund got a full refund due to other reasons. For example if they had a different ticket like the landesticket or if they could not use their ticket due to medical reasons or a semester abroad. A list of theses students (list of matriculation numbers) is also imported.

The system now checks if the people who were sucessfully authenticated by the university, are eligible based on the fact that the system contains a matriculation number for them (staff des not have one) and the pre imported data (the refundin process took longer than the semester which meant there could be students who were not enrolles at the conerned time). Students that were in fact eligible to apply are allowed to enter sepa data which is then stored with their account data. They will also receive a confirmation mail if they have a mailadress set in their university account. Students who were not eligilbe for a refund would get an error.

After applying for the refund the students were listed in the backend where staff members were able to create collective sepa transfers which were exportable as sepa xml files. These could then be imported into banking software. The application automatically set all required fields including the amount which is based on the students data.
When creating the sepa xml file the affected student would again be contacted via mail and informed that the money would be wired shortly.

## Installation

### Debian/Ubuntu

Installation of python 3 in the development version and some dependencies.
For default installation you need a working mysql-database which is accessible from the host.
If you want to switch to sqlite or postgres please see django documentation for necessary settings: <https://docs.djangoproject.com/en/4.0/ref/databases/>

```bash
apt install python3-dev python3-pip default-libmysqlclient-dev build-essential npm xmlsec1 python3-venv
```

In any directory (e.g. /opt) call `git clone` for the repository.
Go into the directory

Create a virtual Python environment (e.g. venv)

```bash
python3 -m venv venv
```

and activate the environment and install dependencies.

```bash
python3 venv/bin/activate
pip3 install -r requirements.txt
```

Now copy the example.env file and edit the necessary settings:

```bash
cp example.env .env
nano .env
```

make database migrations (gather db data and create tables), collect the static files and create a superuser

```bash
./manage.py makemigrations sepa
./manage.py makemigrations app
./manage.py migrate
./manage.py collectstatic
./manage.py createsuperuser
```

you also have to create a private and public key for SAML-SP

```bash
cd sylt
openssl req -nodes -new -x509 -newkey rsa:4096 -days 3650 -keyout private.key -out public.pem
```

Now you should be able to start the development server (never use it for production)

```bash
./manage.py runserver 0.0.0.0:8000
```

If this works you need to configure nginx and your uwsgi-Server, import your studis and your good to go.

## Development

This project uses ![djago-bulma](https://pypi.org/project/django-bulma/).
To work on the CSS (SASS) you have to edit the file `app/static/bulma/style.scss`.
The best way to do this is to start:

``bash
python3 ./manage.py bulma start
```

to recompile the customized theme every time you change it.
