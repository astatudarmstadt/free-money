"""
SEPA views for managing the sepa xml files

"""
from app.models import Studi
from .models import XmlFile
from django.conf import settings
from django.contrib.auth.models import User
from django.core.mail import send_mail
from django.db.models import Sum
from django.shortcuts import render, get_object_or_404
from django.contrib.admin.views.decorators import staff_member_required
from django.template.loader import get_template
from django.template import Context
from sepaxml import SepaTransfer
from slugify import slugify
import datetime as dt
from django.http import HttpResponse
import hashlib

# method to send mails
def send(subject, msg, recipient):

    send_mail(
        subject,
        msg,
        'service@asta.tu-darmstadt.de',
        [recipient],
    )

# method for view to mamage the sepa xml files
# restricted to staff
@staff_member_required(redirect_field_name='index:index', login_url='admin:login')
def xml_constructor(request):
    xml = XmlFile.objects.all()
    # in case of post check for post data
    if request.method == "POST":
        data = request.POST
        # mark xml file as wired by setting the current date and time
        if "wired" in data:
            sepa_id = data.get("sepa_id")
            mark_xml = xml.get(pk=sepa_id)
            if mark_xml:
                mark_xml.wire_date = dt.datetime.now()
                mark_xml.save()
        # create xml-files from waiting transfers
        elif "create" in data:
            transfer_studis = Studi.objects.filter(iban__isnull=False).exclude(sepa_file__isnull=False).exclude(fullrefund=True).order_by('matriculation_number')[:settings.SEPA_TRANSFER_COUNT]
            full_amount = 0
            # start process only if the query received student data
            if transfer_studis:
                # reset bulk and sepa data
                # bulk is needed to update student-data after xml file is created
                bulk=[]
                # collection of sepa transfer data
                sepa = SepaTransfer(settings.SEPA_CONFIG, clean=True)
                # go through all queried data and create sepa 
                count = 0
                for data in transfer_studis.all():
                    description = settings.SEPA_VERWENDUNGSZWECK
                    # we use a transfer hash in the purpose field based on the matriuclation number
                    # may allow us to trace wire transfers in case of errors without releasing unecessary personal data
                    transfer_hash = hashlib.md5((str(data.matriculation_number) + settings.SEPA_SECRET).encode('utf-8')).hexdigest()
                    description += transfer_hash
                    # get amount for transferral, medtec students get more money
                    amount = settings.SEPA_STDAMOUNT
                    if data.medizintechnik:
                        amount = settings.SEPA_VARAMOUNT
                    # create the acutal payment
                    payment = {
                         "name": data.ac_owner,
                         "IBAN": data.iban,
                         "BIC": data.bic,
                         "amount": amount,
                         "execution_date": dt.date.today() + dt.timedelta(days=2),
                         "description": description,
                         # "endtoend_id": str(uuid.uuid1())  # optional
                    }
                    # calculate the full amount of money that will be in the collective transfer
                    full_amount += amount
                    count += 1
                    # add new sepa transfer to sepa file
                    sepa.add_payment(payment)
                    # add the hash to the student
                    data.token_hash = transfer_hash
                    # add the student to the bulk
                    bulk.append(data)
                    # try to send user a confirmation that he will receive the money soon (works only if student has mail)
                    try:
                        user_mail = User.objects.get(username=data.matriculation_number).email
                    except:
                        # do nothing
                        print("no mail")
                    else:
                        if user_mail:
                            mail_text = get_template('app/mail_wired.txt')
                            mail_context = { 'first_name': data.first_name, 'short_iban': data.iban[-4:] }
                            mail_content = mail_text.render(mail_context)
                            subject = "9Euro Ticket - Refund transferred | Rückerstattung überwiesen"
                            send( subject, mail_content, user_mail )
                # create the xml file and save it to db
                new_xml = XmlFile(content=sepa.export(validate=True),amount=full_amount,count=count)
                new_xml.save()
                # update the students
                for i in bulk:
                    i.sepa_file = new_xml
                Studi.objects.bulk_update(bulk,['sepa_file','token_hash'])
    context = {
        'xml_file': xml,
        'count': Studi.objects.filter(iban__isnull=False).exclude(sepa_file_id__isnull=False).exclude(fullrefund=True).count(),
        'transferred_count': xml.aggregate(Sum('count'))['count__sum'],
        'transferred_amount': xml.aggregate(Sum('amount'))['amount__sum'],
        'open_transfers': Studi.objects.filter(iban__isnull=False).exclude(sepa_file_id__isnull=False).exclude(fullrefund=True).order_by('matriculation_number')[:settings.SEPA_TRANSFER_COUNT],
        'sepa_count': settings.SEPA_TRANSFER_COUNT
    }
    return render(request, 'sepa/xml_constructor.html', context)

@staff_member_required(redirect_field_name='index:index', login_url='admin:login')
def stats(request):
    xml = XmlFile.objects.all()
    context = {
        'studentcount_all': Studi.objects.count(),
        'studentcount_nonfullrefund': Studi.objects.filter(fullrefund=False).count(),
        'studentcount_nonfullrefund_nonmedtec': Studi.objects.filter(medizintechnik=False).exclude(fullrefund=True).count(),
        'studentcount_nonfullrefund_medtec': Studi.objects.filter(medizintechnik=True).exclude(fullrefund=True).count(),
        'studentcount_fullrefund': Studi.objects.filter(fullrefund=True).count(),
        'studentcount_fullrefund_nonmedtec': Studi.objects.filter(medizintechnik=False).exclude(fullrefund=False).count(),
        'studentcount_fullrefund_medtec': Studi.objects.filter(medizintechnik=True).exclude(fullrefund=False).count(),
        'studentcount_nonpaid_closed': xml.aggregate(Sum('count'))['count__sum'] - Studi.objects.filter(sepa_file_id__isnull=False).count(),
        'studentcount_nonpaid_open': Studi.objects.filter(iban__isnull=False).exclude(sepa_file_id__isnull=False).exclude(fullrefund=True).count(),
        'studentcount_refunded': Studi.objects.filter(sepa_file_id__isnull=False).count(),
        'studentcount_refunded_nonmedtec': Studi.objects.filter(sepa_file_id__isnull=False).exclude(medizintechnik=True).count(),
        'studentcount_refunded_medtec': Studi.objects.filter(sepa_file_id__isnull=False).exclude(medizintechnik=False).count(),
        'transferred_count': xml.aggregate(Sum('count'))['count__sum'],
        'transferred_amount': xml.aggregate(Sum('amount'))['amount__sum'],
    }
    return render(request, 'sepa/stats.html', context);

# view for downloading xml files from the db
# also only for staff
@staff_member_required(redirect_field_name='index:index', login_url='admin:login')
def download_xml(request, pk):
    # get xml file from database - 404 if it does not exists
    xml_file = get_object_or_404(XmlFile, pk=pk)
    # name filname with creation date and time (hh-mm-ss no milliseconds)
    filename = slugify(str(xml_file.creation_date))[0:19] + "_sepa.xml"
    # return xml file
    return HttpResponse(xml_file.content, headers={'Content-Type': 'application/xml', 'Content-Disposition': 'attachment; filename="{}"'.format(filename)})
