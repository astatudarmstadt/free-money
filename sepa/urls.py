"""
URLs for sepa backend

"""
from django.urls import path
from django.contrib.auth import views as auth_views
from django.conf import settings

from . import views


urlpatterns = [
    path('sepa', views.xml_constructor, name='xml_constructor'),
    path('stats', views.stats, name='stats'),
    path('download_xml/<int:pk>', views.download_xml, name='download_xml')
]