"""
Model for sepa transactions
Contains the sepa xml data and metadata

"""
from django.db import models

# Create your models here.
class XmlFile(models.Model):
    content = models.BinaryField(max_length=104857600)
    creation_date = models.DateTimeField(auto_now_add=True)
    wire_date = models.DateTimeField(null=True)
    amount = models.IntegerField()
    count = models.IntegerField()

    def __str__(self):
        return self.creation_date.isoformat()

    objects = models.Manager()