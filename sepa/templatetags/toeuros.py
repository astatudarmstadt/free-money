from django import template
from django.template.defaultfilters import stringfilter

register = template.Library()


@register.filter
@stringfilter
def toeuros(value):
    return "%.2f" % (int(value)/100)