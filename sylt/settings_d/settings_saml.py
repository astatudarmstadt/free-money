## SAML settings 

LOGIN_REDIRECT_URL = "/details"
LOGIN_URL = '/saml2/login'
LOGOUT_REDIRECT_URL = "/logout"
AUTHENTICATION_BACKENDS = ['django.contrib.auth.backends.ModelBackend', 'djangosaml2.backends.Saml2Backend']
SAML_ACS_FAILURE_RESPONSE_FUNCTION = 'app.views.failure'

SAML_SESSION_COOKIE_NAME = 'saml_session'
SESSION_COOKIE_SECURE = True
SESSION_EXPIRE_AT_BROWSER_CLOSE = True
SAML_CREATE_UNKNOWN_USER = True
SAML_USE_NAME_ID_AS_USERNAME = False
SAML_DJANGO_USER_MAIN_ATTRIBUTE = 'username'

from os import path
import saml2
import saml2.saml
BASEDIR = path.dirname(path.abspath(__file__))

SAML_DEFAULT_BINDING = saml2.BINDING_HTTP_REDIRECT
SAML_LOGOUT_REQUEST_PREFERRED_BINDING = saml2.BINDING_HTTP_REDIRECT
SAML_HOST_URL = str(os.getenv('SAML_HOST_URL'))
SAML_IDP_URL = str(os.getenv('SAML_IDP_URL'))

SAML_CONFIG = {
  # full path to the xmlsec1 binary programm
  'xmlsec_binary': '/usr/bin/xmlsec1',

  # your entity id, usually your subdomain plus the url to the metadata view
  'entityid': SAML_HOST_URL + '/metadata/',

  # directory with attribute mapping
  'attribute_map_dir': path.join(BASEDIR, 'attribute-maps'),

  # Permits to have attributes not configured in attribute-mappings
  # otherwise...without OID will be rejected
  'allow_unknown_attributes': True,

  # this block states what services we provide
  'service': {
      'sp' : {
          'name': str(os.getenv('SAML_SYSTEM_NAME')),
          'name_id_format': saml2.saml.NAMEID_FORMAT_TRANSIENT,

          # For Okta add signed logout requests. Enable this:
          # "logout_requests_signed": True,

          'endpoints': {
              # url and binding to the assetion consumer service view
              # do not change the binding or service name
              'assertion_consumer_service': [
                  (SAML_HOST_URL + '/acs/',
                   saml2.BINDING_HTTP_POST),
                  ],
              # url and binding to the single logout service view
              # do not change the binding or service name
              'single_logout_service': [
                  # Disable next two lines for HTTP_REDIRECT for IDP's that only support HTTP_POST. Ex. Okta:
                  (SAML_HOST_URL + '/ls/',
                   saml2.BINDING_HTTP_REDIRECT),
                  (SAML_HOST_URL + '/ls/post',
                   saml2.BINDING_HTTP_POST),
                  ],
              },

          'signing_algorithm':  saml2.xmldsig.SIG_RSA_SHA256,
          'digest_algorithm':  saml2.xmldsig.DIGEST_SHA256,

           # Mandates that the identity provider MUST authenticate the
           # presenter directly rather than rely on a previous security context.
          'force_authn': False,

           # Enable AllowCreate in NameIDPolicy.
          'name_id_format_allow_create': True,

           # attributes that this project need to identify a user
          'required_attributes': ['urn:oid:2.5.4.42',
                                  'urn:oid:1.3.6.1.4.1.8301.4.2.1.2.1',
                                  'urn:oid:2.5.4.4'],


           # attributes that may be useful to have but not required
          'optional_attributes': [
              'urn:oid:0.9.2342.19200300.100.1.3',
          ],

          'want_response_signed': True,
          'authn_requests_signed': True,
          'logout_requests_signed': True,
          # Indicates that Authentication Responses to this SP must
          # be signed. If set to True, the SP will not consume
          # any SAML Responses that are not signed.
          'want_assertions_signed': True,

          'only_use_keys_in_metadata': True,

          # When set to true, the SP will consume unsolicited SAML
          # Responses, i.e. SAML Responses for which it has not sent
          # a respective SAML Authentication Request.
          'allow_unsolicited': False,

          # in this section the list of IdPs we talk to are defined
          # This is not mandatory! All the IdP available in the metadata will be considered instead.
          'idp': {
              # we do not need a WAYF service since there is
              # only an IdP defined here. This IdP should be
              # present in our metadata

              # the keys of this dictionary are entity ids
              SAML_IDP_URL + '/metadata': {
                  'single_sign_on_service': {
                      saml2.BINDING_HTTP_REDIRECT: SAML_IDP_URL + '/profile/SAML2/Redirect/SSO',
                      },
                  'single_logout_service': {
                      saml2.BINDING_HTTP_REDIRECT: SAML_IDP_URL + '/profile/SAML2/Redirect/SLO',
                      },
                  },
              },
          },
      },

  # where the remote metadata is stored, local, remote or mdq server.
  # One metadatastore or many ...
  'metadata': {
      'remote': [{"url": SAML_IDP_URL + "/metadata"},],
      },

  # set to 1 to output debugging information
  'debug': 1,

  # Signing
  'key_file': path.join(BASEDIR, 'private.key'),  # private part
  'cert_file': path.join(BASEDIR, 'public.pem'),  # public part

  # Encryption
  'encryption_keypairs': [{
      'key_file': path.join(BASEDIR, 'private.key'),  # private part
      'cert_file': path.join(BASEDIR, 'public.pem'),  # public part
  }],

  # own metadata settings
  'contact_person': [
      {'given_name': str(os.getenv('SAML_CONTACT_FIRST_NAME')),
       'sur_name': str(os.getenv('SAML_CONTACT_NAME')),
       'company': str(os.getenv('SAML_CONTACT_ORGANIZATION')),
       'email_address': str(os.getenv('SAML_CONTACT_MAIL')),
       'contact_type': str(os.getenv('SAML_CONTACT_TYPE'))},
      ],
  # you can set multilanguage information here
  'organization': {
      'name': [(str(os.getenv('SAML_ORGANIZATION_NAME')), str(os.getenv('LANGUAGE')))],
      'display_name': [(str(os.getenv('SAML_ORGANIZATION_DISPLAY_NAME')), str(os.getenv('LANGUAGE')))],
      'url': [(str(os.getenv('SAML_ORGANIZATION_URL')), str(os.getenv('LANGUAGE')))],
      },
  }

# map saml attributes to user attributes
# most important: our username is the matriculation number
SAML_ATTRIBUTE_MAPPING = {
    str(os.getenv('SAML_ATTRIBUTE_MAPPING_USERNAME')): ('username', ),
    str(os.getenv('SAML_ATTRIBUTE_MAPPING_EMAIL')): ('email', ),
    str(os.getenv('SAML_ATTRIBUTE_MAPPING_FIRST_NAME')): ('first_name', ),
    str(os.getenv('SAML_ATTRIBUTE_MAPPING_LAST_NAME')): ('last_name', ),
}
