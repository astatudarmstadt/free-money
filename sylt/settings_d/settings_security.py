## security related settings

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = str(os.getenv('SECRET_KEY'))

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# setting for sanity checks
# send a warning mail if iban is used more than the set amount (e.g. if set to one a mail goes out if used twice). If set to 0 the setting is disabled.
WARNING_IBAN_USED = str(os.getenv('WARNING_IBAN_USED'))
WARNING_IBAN_MAIL = str(os.getenv('WARNING_IBAN_MAIL'))

# settings for web-security
PROTOCOL = str(os.getenv('PROTOCOL'))
DOMAIN = str(os.getenv('DOMAIN'))
ALLOWED_HOSTS = [DOMAIN]
CSRF_COOKIE_DOMAIN = DOMAIN
CSRF_TRUSTED_ORIGINS = [PROTOCOL + '://' + DOMAIN]
CSRF_COOKIE_HTTPONLY = True
CSRF_COOKIE_SAMESITE = "Strict"
CSRF_COOKIE_SECURE = True
CSRF_COOKIE_NAME = '__Secure-csrftoken'
SECURE_BROWSER_XSS_FILTER = True
SECURE_HSTS_SECONDS = 31536000
SECURE_HSTS_INCLUDE_SUBDOMAINS = True
SECURE_HSTS_PRELOAD = True
SECURE_CONTENT_TYPE_NOSNIFF = True
SECURE_REFERRER_POLICY = "same-origin"
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')
SECURE_SSL_REDIRECT = True
X_FRAME_OPTIONS = 'DENY'
SESSION_COOKIE_NAME = '__Secure-sessionid'

## settings for ratelimiting

# enable ratelimiting
RATELIMIT_ENABLE = True
# show this page in case user gets blocked
RATELIMIT_VIEW = 'app.views.ratelimit'


# Password validation
# https://docs.djangoproject.com/en/4.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]
