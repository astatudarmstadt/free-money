## mailsettings

# send mail via smtp
EMAIL_BACKEND = str(os.getenv('EMAIL_BACKEND'))
EMAIL_HOST = str(os.getenv('EMAIL_HOST'))
EMAIL_PORT = int(str(os.getenv('EMAIL_PORT')))
EMAIL_USE_TLS = str(os.getenv('EMAIL_USE_TLS'))
EMAIL_HOST_USER = str(os.getenv('EMAIL_HOST_USER'))
EMAIL_HOST_PASSWORD = str(os.getenv('EMAIL_HOST_PASSWORD'))
DEFAULT_FROM_EMAIL = str(os.getenv('DEFAULT_FROM_EMAIL'))
