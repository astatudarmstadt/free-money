## settings for sepa wire transfers

# config for wiretransfer
SEPA_CONFIG = {
    "name": str(os.getenv('SEPA_CONFIG_NAME')),
    "IBAN": str(os.getenv('SEPA_CONFIG_IBAN')),
    "BIC": str(os.getenv('SEPA_CONFIG_BIC')),
    "batch": True,
    # For non-SEPA transfers, set "domestic" to True, necessary e.g. for CH/LI
    "currency": "EUR",  # ISO 4217
}

# frist part of the "Verwendungszweck" - second part is hash of matriculation number and secret
SEPA_VERWENDUNGSZWECK = str(os.getenv('SEPA_VERWENDUNGSZWECK'))

# Secret used to create hash for wire transfer
SEPA_SECRET = str(os.getenv('SEPA_SECRET'))

# amount in cents for reimbursement. second amount is for students where medizintechnik == True
SEPA_STDAMOUNT = int(os.getenv('SEPA_STDAMOUNT'))
SEPA_VARAMOUNT = int(os.getenv('SEPA_VARAMOUNT'))

# amount of maximal transfers per xml-file
SEPA_TRANSFER_COUNT = int(os.getenv('SEPA_TRANSFER_COUNT'))
