"""
URLs for main app

"""
from django.urls import path, include

from . import views


urlpatterns = [
    path('', views.index, name='index'),
    path('details', views.StudiUpdateView.as_view(), name='details'),
    path('finished', views.finished, name='finished'),
    path('denied', views.denied, name='denied'),
    path('logout', views.user_logout, name='logout'),
    path('i18n/', include('django.conf.urls.i18n')),
]
