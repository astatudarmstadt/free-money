"""
Admin models for viewing data in the backend

Most models are read-only
"""

from django.contrib import admin
from sepa.models import XmlFile

from .models import Studi, History


# show empty values as -leer- in the backend
# TODO translate
admin.site.empty_value_display = '-leer-'


class HistoryAdminInline(admin.TabularInline):
    model = History


class StudiAdmin(admin.ModelAdmin):
    """
    View student entries.
    """
    # it is not allowed to delete anything
    def has_delete_permission(self, request, obj=None): return False
    # some data is read-only, even in backend
    def get_readonly_fields(self, request, obj=None):
        if obj:
            return [
                'matriculation_number',
                'last_name',
                'first_name',
                'medizintechnik',
                'fullrefund',
                'sepa_file',
                'first_entry',
                'last_change',
                'token_hash',
                'version'
            ]
        return self.readonly_fields
    list_display = (
        'matriculation_number',
        'last_name',
        'first_name',
        'iban',
        'sepa_file'
    )
    list_filter = (('sepa_file', admin.EmptyFieldListFilter), 'version', ('email', admin.EmptyFieldListFilter))
    search_fields = [
        'matriculation_number',
        'last_name',
        'first_name',
        'iban',
        'first_entry',
        'last_change',
        'token_hash'
    ]
    raw_id_fields = ('sepa_file',)
    inlines = (HistoryAdminInline, )


class SepaAdmin(admin.ModelAdmin):
    """
    View sepa xml-fileentries.
    """
    # read-only
    def has_change_permission(self, request, obj=None): return False
    def has_delete_permission(self, request, obj=None): return False

    list_display = ('creation_date', 'wire_date')


class HistoryAdmin(admin.ModelAdmin):
    """
    View history entries.
    """
    # read-only
    def has_change_permission(self, request, obj=None): return False
    def has_delete_permission(self, request, obj=None): return False

    list_display = ('user_name', 'studi', 'date')
    search_fields = ['user_name', 'studi']


admin.site.register(Studi, StudiAdmin)
admin.site.register(XmlFile, SepaAdmin)
admin.site.register(History, HistoryAdmin)
