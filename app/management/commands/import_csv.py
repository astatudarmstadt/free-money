"""
Command to import students. CSV file needs to be provided by the university

CSV columns:
filename of a csv file which contain matriculations_numbers,
vorname, nachname, and a boolean field for medizintechnik.
"""

from django.core.management.base import BaseCommand
from app.models import Studi
import pandas as pd


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('filename', nargs=1, type=str)

    def handle(self, *args, **options):
        filename = options['filename'][0]
        dbframe = pd.read_csv(filename, encoding='utf-8')
        for dbframe in dbframe.itertuples():
            obj, created = Studi.objects.update_or_create(
                matriculation_number=dbframe.matrikelnummer,
                defaults={
                    'first_name': dbframe.vorname,
                    'last_name': dbframe.nachname,
                    'medizintechnik': dbframe.medizintechnik,
                })
            obj.save()
            print('created' if created else 'updated', obj)
        print('done')
