"""
Form for iban entry

"""
from django import forms
from django.utils.translation import gettext as _

from localflavor.generic.forms import IBANFormField, BICFormField
from localflavor.generic.countries.sepa import IBAN_SEPA_COUNTRIES


class AccountInfoForm(forms.Form):
    ac_owner = forms.CharField(label=_(u'ac_owner'), max_length=100)
    iban = IBANFormField(label=_(u'iban'), include_countries=IBAN_SEPA_COUNTRIES)
    bic = BICFormField(label=_(u'bic'))
