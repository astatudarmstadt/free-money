"""
Models for app frontend

Contains models studi and history which contains documentation of changes
to the class Studi.
"""
import datetime as dt

from django.db import models
from django.conf import settings
from localflavor.generic.models import BICField, IBANField
from localflavor.generic.countries.sepa import IBAN_SEPA_COUNTRIES
from django.core.validators import RegexValidator
from django.utils.translation import gettext as _
from sepa.models import XmlFile

sepa_validator = RegexValidator(regex='^[A-Za-z0-9\s\/\-\?:().,‚+]*$', message="Due to int. sepa-specifications only letters, numbers, space and /-?:().,‚+ allowed. Please change mutated vowels like ä, ö and ü to ae, oe and ue and ß to ss.", code="invalid_accountowner_name")

class Studi(models.Model):
    """
    Base class containig students identified by matriculation number.
    """
    matriculation_number = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=150)
    last_name = models.CharField(max_length=100)
    email = models.CharField(max_length=100, null=True, blank=True)
    medizintechnik = models.BooleanField(default=False)
    fullrefund = models.BooleanField(default=False)
    sepa_file = models.ForeignKey(XmlFile, null=True, on_delete=models.RESTRICT)
    first_entry = models.DateTimeField(null=True, blank=True)
    last_change = models.DateTimeField(null=True, blank=True)
    ac_owner = models.CharField(max_length=30, null=True, verbose_name=_('ac_owner'), validators=[sepa_validator])
    iban = IBANField(include_countries=IBAN_SEPA_COUNTRIES, null=True, verbose_name='IBAN')
    bic = BICField(null=True, verbose_name='BIC')
    token_hash = models.CharField(max_length=32, null=True)
    version = models.IntegerField(default=0)

    # save has been extended to write history entries on change
    def save(self, *args, **kwargs):
        # if newly created object (import) don't create a history
        try:
            # On save, update timestamps and history
            studi = Studi.objects.get(pk=self.matriculation_number)
            if not self.first_entry:
                self.first_entry = dt.datetime.now()
            self.last_change = dt.datetime.now()
            self.version += 1
            history = History(
                date=dt.datetime.now(),
                user_ip=self.ip,
                user_name=self.user,
                studi=self,
                changed_fields={
                    'ac_owner': self.ac_owner,
                    'IBAN': self.iban,
                    'BIC': self.bic
                }
            )
            history.save()
            return super(Studi, self).save(*args, **kwargs)
        except:
            return super(Studi, self).save(*args, **kwargs)


class History(models.Model):
    """
    Implement a student history to comprehend changes.
    """
    date = models.DateTimeField()
    user_ip = models.CharField(max_length=40)
    user_name = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        null=True
    )
    changed_fields = models.CharField(max_length=500)
    studi = models.ForeignKey(Studi, on_delete=models.RESTRICT)

    def __str__(self):
        return str(self.user_name)

    objects = models.Manager()
