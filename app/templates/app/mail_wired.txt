Hallo,

- English version below - 

Die Überweisung Deines Rückerstattungsantrages zum 9Euro Ticket wurde heute in Auftrag gegeben. Das Geld sollte in den nächsten 14 Tagen auf Deinem Konto eingehen.

Die empfangende IBAN endet auf: ****{{ short_iban }}

Liebe Grüße
Dein AStA


- English version -

the transfer of your refund request for the 9Euro ticket was ordered today. The money should arrive in your account within the next 14 days.

The receiving IBAN ends with: ****{{ short_iban }}

Kind regards
Your AStA


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

AStA TU Darmstadt
Hochschulstraße 1
64289 Darmstadt
9euro@asta.tu-darmstadt.de

Homepage: asta.tu-darmstadt.de
Twitter: twitter.com/astatudarmstadt
Instagram: instagram.com/astatuda/
Facebook: facebook.com/AStA.TUDarmstadt/
Telegram-Channel: t.me/tuasta
