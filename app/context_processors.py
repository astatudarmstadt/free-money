"""
Processor to import mailadresse from settings into the templates

"""
from django.conf import settings


def service_email(request):
    return {
        'SERVICE_EMAIL': settings.SERVICE_EMAIL
    }

def common_links(request):
    return {
        'IMPRINT_LINK': settings.IMPRINT_LINK,
        'PRIVACY_POLICY_LINK': settings.PRIVACY_POLICY_LINK
    }


